use QM;

my @regular-minterms = 0, 1, 2, 5, 6, 7, 8, 9, 10, 14;
my @dont-care = []; 

# the don't care conditions array is optional.
my $qm = QM.minterms(@regular-minterms, @dont-care);

put $qm.all-minterms.join(', ');      #=> 0, 1, 2, 5, 6, 7, 8, 9, 10, 14

put $qm.prime-implicants<all>;        #=> 011- 0-01 01-1 --10 -00- -0-0

put $qm.essential-prime-implicants;   #=> --10 -00-

put $qm.prime-implicants-chart.perl;
#=>
#`{
"0" => $["-00-", "-0-0"],
"1" => $["-00-", "0-01"],
"10" => $["--10", "-0-0"],
"14" => $["--10"],
"2" => $["--10", "-0-0"],
"5" => $["01-1", "0-01"],
"6" => $["--10", "011-"],
"7" => $["011-", "01-1"],
"8" => $["-00-", "-0-0"],
"9" => $["-00-"]
}

put $qm.prime-implicants-chart(:dec).perl;
#=> 
# ([2, 6, 10, 14], [0, 2, 8, 10], [0, 1, 8, 9], [1, 5], [5, 7], [6, 7]).Seq

put $qm.reduced-boolean-expression;  #=> A'BD + CD' + B'C'
