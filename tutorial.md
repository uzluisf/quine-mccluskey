The minimization of boolean functions can be done through various simplification techniques. Some of them are:

  * **Boolean algebra** - useful and simple to simplify boolean expressions but it's not always clear when the minimal solution is reached.

  * **Karnaugh maps** - great to get a minimal solution but it becomes cumbersome with 5 or more variables.

However, we won't discuss them here. Instead, we're interested on the **Quine-McCluskey** algorithm which is another simplification technique.

The **Quine-McCluskey** algorithm is a simplification technique that allows us to systematically reach a minimal solution using a system of any `n` variables.

Procedure
---------

The producedure is as follows:

1. Start with a minterm expansion for a boolean function `F`.

2. Eliminate as many literals from each term by applying the following property of Boolean algebra: `XY + XY' = X(Y + Y') = X(1) = X`.

3. Use a *prime implicant chart* to select the minimal set of implicants.

Algorithm
---------

1. List all the minterms available for the given function (e.g., a three-variable functions has `2³ = 8` minterms). However, they must be grouped by the number of 1s required to represent them. For instance, for a four-variable function, `0000` belongs to the group `0`.

2. Compare the terms in one group with the terms in the adjacent group to see if the terms can be combined (e.g., groups `0` and `1` are adjacents so their terms are compared).

2.1. Place combinations in the next table's column with a dash to represent the variable eliminated.

2.2. Check each term that was combined. Those terms that couldn't be combined and are thus unchecked are the prime implicants for that specific table.

3. Repeat the same algorithm in the next table, comparing terms until not further simplification can be made.

Example
-------

Reduce `F(A, B, C, D) = ∑(0, 1, 2, 3, 5, 6, 7, 8, 9, 10, 14)` using the QM algorithm.

### Groups

First iteration: 

    Group No.	Minterms	Binary of Minterms
    ==================================================
        0:           0                       0000 ✓
    --------------------------------------------------
        1:           1                       0001 ✓
                     2                       0010 ✓
                     8                       1000 ✓
    --------------------------------------------------
        2:           5                       0101 ✓
                     6                       0110 ✓
                     9                       1001 ✓
                     10                      1010 ✓
    --------------------------------------------------
        3:           7                       0111 ✓
                     14                      1110 ✓
    --------------------------------------------------

Unchecked terms: None

All terms are checked, which means we were able to combine them into new terms which will be in the minterms of the next table.

Second iteration: 

    Group No.    Minterms   Binary of Minterms
    ==================================================
        0:          0,1                     000- ✓
    --------------------------------------------------
                    0,2                     00-0 ✓
                    0,8                     -000 ✓
        1:          1,5                     0-01
    --------------------------------------------------
                    1,9                     -001 ✓
                    2,6                     0-10 ✓ 
                    2,10                    -010 ✓
                    8,9                     100- ✓
                    8,10                    10-0 ✓
    --------------------------------------------------
        2:          5,7                     01-1
                    6,7                     011-
                    6,14                    -110 ✓
                    10,14                   1-10 ✓
    --------------------------------------------------

Unchecked terms: 011-, 0-01, 01-1

Third iteration: 

    Group No.	Minterms	Binary of Minterms
    ==================================================
        0:      0,1,8,9                 -00-
                0,1,8,9                 -00-
                0,2,8,10                -0-0 
                0,2,8,10                -0-0
    --------------------------------------------------
        1:      2,6,10,14               --10
                2,6,10,14               --10
    --------------------------------------------------

Unchecked terms: -0-0, -00-, --10

Sometimes terms may repeat in a column in which case we take only a copy since the other copies are redundant.

### Prime implicants

The following are all the prime implicants we obtained in the grouping phase:

These terms are the only ones that will appear in the minimal sum of product (SoP) expression of the boolean function. To determine which ones will get used, we must determine which ones are essential, and then pick from the rest the terms to cover all minterms in the simplest way. In order to determine this, we must use a prime implicant chart.

### Prime implicant Chart (PIC)

                  | 0  1  2  5  6  7  8  9  10  14
    ——————————————————————————————————————————————
    (0,1,8,9)     | X  X              X  E        
    (0,2,8,10)    | X     X           X      X
    (2,6,10,14)   |       X     X            X   E
    (1,5)         |    X     X         
    (5,7)         |          X     X   
    (6,7)         |             X  X

As you can see, creating the PIC is only a matter of listing the prime implicants, the minterms and drawing an `X` to indicate the minterms each prime implicant covers. From this chart, we can easily see which prime implicants are essential. We use to `E` here to showcase them.

#### Using the PIC

  * Each time a prime implicant is selected for inclusion in the minimal SoP, the corresponding row should be crossed out.

  * The columns which correspond to all minterms covered by that prime implicant should also be crossed out.

  * Start by selecting the essential prime implicants.

First EPI:

                  | 0  1  2  5  6  7  8  9  10  14
    ——————————————————————————————————————————————
    (0,1,8,9)     | X--X--------------X--E--------
    (0,2,8,10)    | X  |  X           X      X
    (2,6,10,14)   | |  |  X     X     |      X   E
    (1,5)         | |  X     X        |
    (5,7)         | |  |     X     X  |
    (6,7)         | |  |        X  X  |

Second EPI:

                  | 0  1  2  5  6  7  8  9  10  14
    ——————————————————————————————————————————————
    (0,1,8,9)     | X--X--|-----------X--E---|----
    (0,2,8,10)    | X  |  X           X      X
    (2,6,10,14)   | |  |  X-----X-----|------X---E
    (1,5)         | |  X  |  X  |     |      |
    (5,7)         | |  |  |  X  |  X  |      |
    (6,7)         | |  |  |     X  X  |      |

Now we're with two prime implicants and we must choose the one that covers the most the most minterms.

PI:

                  | 0  1  2  5  6  7  8  9  10  14
    ——————————————————————————————————————————————
    (0,1,8,9)     | X--X--|-----------X--E---|----
    (0,2,8,10)    | X  |  X           X      X
    (2,6,10,14)   | |  |  X--|--X--|--|------X---E
    (1,5)         | |  X  |  X  |  |  |      |
    (5,7)         | |--|--|--X--|--X--|------|----
    (6,7)         | |  |  |  |  X  X  |      |

We're left with two essential prime implicants and one prime implicant. Thus, `F = (0, 1, 8, 9) + (2, 6, 10, 14) + (5, 7) = -00- + --10 + 011-`.

The reduced boolean expression for F is `B'C' + CD' + A'BD`.

