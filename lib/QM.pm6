use Utils;

#|«
Class that implements the Quine-McCluskey algorithm for boolean expression 
simplification.
»
unit class QM is export;

has @!regular-minterms;
has @!dont-care;

submethod TWEAK( :@regular-minterms, :@dont-care ) {
    my $they-intersect = @regular-minterms ∩ @dont-care;
    if $they-intersect {
        note qq:to/END/;
        » Minterm overlap: {$they-intersect.keys.sort.join(', ')}
        » Using it/them as regular minterm(s).
        END
    }
    @!regular-minterms = @regular-minterms;
    @!dont-care = (@dont-care ∖ @regular-minterms).keys;
}

#|«
Constructor method that takes a first array containing the regular minterms
and an optional second array containing the don't care conditions.
»
method minterms( ::?CLASS:U: @regular-minterms, @dont-care? ) {
    die "✘ You must provide at least 1 minterm" unless @regular-minterms;
    self.bless: :@regular-minterms, :@dont-care;
}

#| Return all minterms, both regular minterms and don't care conditions.
method all-minterms( ::?CLASS:D: --> List:D ) {
    (@!regular-minterms, @!dont-care).flat.sort.list
}

#| Return the reduced boolean expression for the given minterms.
method reduced-boolean-expression( --> Str:D ) {
    my @epi   = self.essential-prime-implicants;
    my @formula;

    # remove columns already covered by the essential prime implicants.
    my %chart = remove-terms(@epi, self.prime-implicants-chart);

    # if no minterms remain after removing EPI related columns from chart, then
    # the reduced boolean expression only includes the essential prime
    # implicants (EPIs)...
    if %chart.elems == 0 {
        @formula = (find-variables($_) for @epi)
    }
    # ... else use Petrick's method for further simplification of 
    # the remaining prime implicants and choose the one(s) that cover the most
    # column(s).
    else {
        # get the variable representation for each of the prime implicants
        # covering the minterms in %chart
        my @P = %chart.values.deepmap: {find-variables($_)};

        # Keep multiplying until we get the non-canonical SoP form of P
        until @P.elems == 1 {
            @P[1] = multiply(@P[0], @P[1]);
            @P.shift;
        }
        # choose the minterm with the less number of literals that cover
        # the remaining columns...
        @formula = @P[0].sort(*.elems).first;
        # ... and add them to the list of EPIs.
        @formula.append: @epi.map({find-variables($_)});
    }

    # function is 1 regardless of the variables's values
    return "1" unless (gather @formula.deepmap: *.take);

    my @products = @formula.grep(* !=== Any)».join('').Set.keys.sort: {
        # ... by characters number
        $^a.subst(:g, /\s*"'"\s*/, "").chars cmp
        $^b.subst(:g, /\s*"'"\s*/, "").chars
            or
        # ... by ascending lexicographical order
        $^a leg $^b
    };

    return @products.join(' + ');
}

#| Return the list of prime implicants.
method prime-implicants( --> Hash:D ) {

    =begin comment
    List all the minterms available for the given function (e.g., a
    three-variable functions has 2³ = 8 minterms). However, they must
    be grouped by the number of 1s required to represent them.
    For instance, for a four-variable function, 0000 belongs to the group 0.

    Group all minterms by the number of 1 bits required to represent them.
    =end comment

    my @all      = self.all-minterms».Int.sort;   # sort all minterms
    my $bits-num = @all.tail.base(2).chars;       # get number of bits per...
    my %groups   = @all.classify(                 # ... number to be used
    { $_.base(2).comb('1').Int },                 # classify by number of 1 bits...
    :as{ "%0*d".sprintf($bits-num, $_.base: 2) }  # ... as binary numbers
    );

    my Set %all-implicants;
    my Int $table-num = 1;

    =begin comment
    2. Compare the terms in one group with the terms in the adjacent group to
    see if the terms can be combined (e.g., groups 0 and 1 are adjacents so
    their terms are compared).
        2.1. Place combinations in the next table's column with a dash to
        represent the variable eliminated.
        2.2. Check each term that was combined. Those terms that couldn't be
        combined and are thus unchecked are the prime implicants for that
        specific table.
    3. Repeat the same algorithm in the next table, comparing terms until not
    further simplification can be made.
    =end comment

    loop {
        my SetHash $checked  .= new;
        my Bool $should-stop  = True;
        my %tmp-groups        = %groups;
        %groups               = %();
    
        my @idx = %tmp-groups.keys.sort;
        for @idx[0..*-2] -> $i {

            # create pair of terms from the current and next groups.
            for %tmp-groups{ $i }.flat X %tmp-groups{ $i+1 }.flat {
                my ($j, $k) = ($_.first, $_.tail);
                my @res = compare($j, $k);

                # if minterms differ by one and only one bit,
                if @res.head {
                    # take any of them and replace differing bit with '-' ...
                    my $minterm = $j;
                    $minterm.substr-rw(@res.tail, 1) = '-';

                    # ... then add combined minterm if isn't grouped already ...
                    if $minterm ∉ %groups{$i} {
                        %groups{$i}.push: $minterm;
                    }

                    $should-stop = False;

                    # ... finally check minterms that were combined
                    $checked{$j}++;
                    $checked{$k}++;
                }
            }
        }

        # collect current column's unchecked minterms, which are prime implicants.
        my $column-unchecked = 
            set(gather %tmp-groups.values.deepmap: *.take) ∖ $checked;

        # add these prime implicants to the list of all prime implicants
        %all-implicants{'table' ~ $table-num} = $column-unchecked;

        $table-num++; 

        # not further simplification can be made
        last if $should-stop;
    }

    # create a hash key for all the prime implicants
    %all-implicants<all> = [∪] %all-implicants.values;

    return %all-implicants;
}

#| Return the essential prime implicants (EPIs).
method essential-prime-implicants( --> List:D ) {
    =begin comment
    Iterate over all minterms and the prime implicants that cover them. If
    a minterm is covered by a single prime implicant (PI), then that PI
    is an essential prime implicant (EPI) and it must be stored if it hasn't
    been stored yet.
    =end comment
    my %pi-chart = self.prime-implicants-chart;
    my @res = gather for %pi-chart.keys.grep(* !=== Any) -> $k {
        if %pi-chart{$k}.elems == 1 {
            if %pi-chart{$k}.first -> $epi {
                $epi.take if $epi ∉ @res
            }
        }
    }

    # sort essential prime implicants by ...
    @res.sort({ 
        .comb('-').Int,        # ... number of dashes
        .flip.indices('-').sum # ... how significant the absent bits are
    }).reverse.list;
}

method prime-implicants-chart( Bool :$dec = False ) {
    my $sorted-pi = self.prime-implicants<all>.keys.list;

    =begin comment
    Return a list of prime implicants, where each implicant is usually
    composed of combined minterms. For instance, the prime implicant 
    -00- corresponds to the array of combined minterms [0, 1, 8, 9].
    =end comment
    if $dec {
        return
        # obtaing minterms in binary form
        (gather $sorted-pi.deepmap: *.take)
        # sort and get unique values
        .sort.unique
        # get the combined minterms
        .map({find-combined-minterms($_)});
    }

    =begin comment
    Return a hash of minterms mapped to arrays of prime implicants that 
    cover them. For instance, the minterm 0 might be covered by the 
    prime implicants -0-0 and -00- and thus the key "0" in the hash maps to
    the array [-0-0, -00-].
    =end comment
 
    my %chart;
    for $sorted-pi.reverse -> $i {
        my @combined-minterms = find-combined-minterms($i);
        # remove the don't care conditions from the list of minterms
        my @minterms-only = (@combined-minterms (-) @!dont-care).keys.sort;

        for @minterms-only -> $j {
            %chart{$j}.push: $i ∉ %chart{$j} ?? $i !! Nil;
        }
    }

    return %chart;
}
